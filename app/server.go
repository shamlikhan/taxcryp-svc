package app

import (
	"taxcryp/app/router"
	"taxcryp/cmd/domain"
	"taxcryp/cmd/handler"
	"taxcryp/cmd/usecase"
	"taxcryp/config"
	"taxcryp/database"
	"taxcryp/logger"
)

func Start() {
	config := config.GetConfig()

	db, err := database.PrepareDatabase()
	if err != nil {
		panic(err)
	}

	_, err = logger.InitLogger(config.Environment)
	if err != nil {
		logger.Errorf("error initializing logger", err)
	}

	dom := domain.InitDomain(db)
	uc := usecase.Init(dom)
	hndlr := handler.Init(uc)

	router := router.PrepareRouter(&router.Capsule{
		DB:      db,
		Domain:  dom,
		Usecase: uc,
		Handler: hndlr,
	})

	logger.Infof("server running at port %s", config.ServerPort)
	err = router.Run(":" + config.ServerPort)
	if err != nil {
		logger.Fatalf("error running server - %s", err.Error())
	}
}
