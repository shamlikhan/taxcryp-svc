package router

import (
	"taxcryp/cmd/domain"
	"taxcryp/cmd/handler"
	"taxcryp/cmd/middleware"
	"taxcryp/cmd/usecase"
	"taxcryp/config"
	"taxcryp/consts"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Capsule struct {
	DB      *gorm.DB
	Domain  *domain.Domain
	Usecase *usecase.Usecase
	Handler *handler.Handler
}

func PrepareRouter(capsule *Capsule) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()

	router.Use(
		gin.LoggerWithWriter(gin.DefaultWriter, "/health"),
		gin.Recovery(),
	)

	config := config.GetConfig()
	if config.Environment != consts.PRODUCTION {
		router.Static("/swaggerui/", "./swagger-ui")
	}

	router.Use(middleware.CORSMiddleware())

	v1 := router.Group("v1")

	capsule.HealthRoutes(v1)
	capsule.SampleRoutes(v1)

	return router
}
