package router

import (
	"taxcryp/cmd/entity/request"
	"taxcryp/cmd/middleware"

	"github.com/gin-gonic/gin"
)

func (c *Capsule) SampleRoutes(r *gin.RouterGroup) {
	sampleHandler := c.Handler.SampleHandler

	sampleV1 := r.Group("/sample")

	sampleV1.GET("", sampleHandler.HandleGetSampleData)
	sampleV1.POST("", middleware.Validate(request.CreateSampleRequest{}), sampleHandler.HandleCreateSampleData)
}
