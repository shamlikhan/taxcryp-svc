package main

import (
	"taxcryp/app"
	_ "taxcryp/docs"
)

func main() {
	app.Start()
}
