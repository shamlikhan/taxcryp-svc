package docs

import (
	"taxcryp/cmd/entity/models"
	"taxcryp/cmd/entity/request"
)

// swagger:route POST /v1/sample Sample idCreateSample
// Creates a Sample
// responses:
// 		200: CreateSampleReponseWrapper
//		400: CustomErrorWrapper

// swagger:parameters idCreateSample
type CreateSampleRequestWrapper struct {
	// in:body
	Body request.CreateSampleRequest
}

// swagger:response CreateSampleReponseWrapper
type CreateSampleReponseWrapper struct {
	// in:body
	Body models.Sample
}

// swagger:route GET /v1/sample Sample idGetSample
// Fetch sample data
// responses:
// 		200: GetSampleResponseWrapper
//		400: CustomErrorWrapper

// swagger:response GetSampleResponseWrapper
type GetSampleResponseWrapper struct {
	// in:body
	Body []models.Sample
}
