package docs

import (
	"taxcryp/cmd/entity/response"
)

// swagger:route GET /health Health idGetHealth
// Checks the health of taxcryp app
// responses:
// 		200: GetHealthResponse

// swagger:response GetHealthResponse
type GetHealthResponseWrapper struct {
	// in:body
	Body response.Health
}
