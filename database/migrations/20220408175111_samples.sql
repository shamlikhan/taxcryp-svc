-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS samples (
	id BIGSERIAL NOT NULL,
	count INT DEFAULT 0,
	PRIMARY KEY (id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE samples;
-- +goose StatementEnd
