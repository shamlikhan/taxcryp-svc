package request

type CreateSampleRequest struct {
	Count int `json:"count" validate:"required"`
}
