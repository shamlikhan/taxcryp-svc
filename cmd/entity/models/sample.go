package models

type Sample struct {
	ID    uint `json:"id" gorm:"primaryKey;autoIncrement"`
	Count int  `json:"count"`
}
