package sample

import (
	"fmt"
	"taxcryp/cmd/entity/models"
	"taxcryp/config"
	"taxcryp/logger"
)

func (d *domain) CreateSampleData(obj models.Sample) (result models.Sample, err error) {
	logger.Info("Begin Domain - CreateSampleData")

	err = d.db.Create(&obj).Error
	if err != nil {
		logger.Error("error creating sample", err.Error())
	}

	logger.Info("End Domain - CreateSampleData - Sample ID = ", obj.ID)
	return obj, err
}

func (d *domain) GetSampleData() (result []models.Sample, err error) {
	logger.Info("Begin Domain - GetSampleData")

	var samples []models.Sample
	config := config.GetConfig()

	tableName := fmt.Sprintf("%s.samples", config.PostgresSchema)
	err = d.db.Table(tableName).Select("*").Find(&samples).Error
	if err != nil {
		logger.Error("error fetching from sample", err.Error())
	}

	logger.Info("End Domain - GetSampleData - Sample Count = ", len(samples))
	return samples, err
}
