package sample

import (
	"taxcryp/cmd/entity/models"

	"gorm.io/gorm"
)

type DomainInterface interface {
	GetSampleData() (result []models.Sample, err error)
	CreateSampleData(obj models.Sample) (result models.Sample, err error)
}

type domain struct {
	db *gorm.DB
}

func InitSampleDomain(db *gorm.DB) DomainInterface {
	return &domain{
		db: db,
	}
}
