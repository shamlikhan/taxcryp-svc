package domain

import (
	"taxcryp/cmd/domain/sample"

	"gorm.io/gorm"
)

type Domain struct {
	Sample sample.DomainInterface
}

func InitDomain(db *gorm.DB) *Domain {
	return &Domain{
		Sample: sample.InitSampleDomain(db),
	}
}
