package middleware

import (
	"taxcryp/cmd/entity/request"
	"taxcryp/logger"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

func Validate(data interface{}) gin.HandlerFunc {
	return func(c *gin.Context) {
		switch data.(type) {
		case request.CreateSampleRequest:
			var createSample request.CreateSampleRequest
			err := c.BindJSON(&createSample)
			if err != nil {
				logger.Errorf("error decoding", "error", err)
			}
			validate := validator.New()
			if err := validate.Struct(&createSample); err != nil {
				c.Set("error", err.Error())
			}
			c.Set("createSampleRequest", createSample)
		}
	}
}
