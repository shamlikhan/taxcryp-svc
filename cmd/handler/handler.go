package handler

import (
	"taxcryp/cmd/usecase"
)

type Handler struct {
	SampleHandler *sampleHandler
	HealthHandler *healthHandler
}

func Init(uc *usecase.Usecase) *Handler {
	return &Handler{
		SampleHandler: InitSampleHandler(uc.Sample),
		HealthHandler: InitHealthHandler(),
	}
}
