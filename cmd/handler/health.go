package handler

import (
	"taxcryp/cmd/entity/response"
	"taxcryp/cmd/utils"

	"github.com/gin-gonic/gin"
)

type healthHandler struct{}

func InitHealthHandler() *healthHandler {
	return &healthHandler{}
}

func (h *healthHandler) HandleGetHealth(c *gin.Context) {
	result := response.Health{
		Status:  "normal",
		Message: "system running normally",
	}
	c.JSON(200, utils.Send(result))
}
