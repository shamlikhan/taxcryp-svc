package handler

import (
	"taxcryp/cmd/entity/request"
	"taxcryp/cmd/usecase/sample"
	"taxcryp/cmd/utils"
	"taxcryp/logger"

	"github.com/gin-gonic/gin"
)

type sampleHandler struct {
	sampleUC sample.UsecaseInterface
}

func InitSampleHandler(uc sample.UsecaseInterface) *sampleHandler {
	return &sampleHandler{
		sampleUC: uc,
	}
}

func (h *sampleHandler) HandleCreateSampleData(c *gin.Context) {
	logger.Info("Begin Handler - CreateSampleData")
	if errMessage, ok := c.Get("error"); ok {
		c.JSON(400, utils.Fail(100, errMessage.(string)))
		return
	}

	data, ok := c.Get("createSampleRequest")
	if !ok {
		c.JSON(400, utils.Fail(100, utils.ErrInvalidRequest.Error()))
		return
	}
	sampleRequest := data.(request.CreateSampleRequest)

	res, err := h.sampleUC.CreateSampleData(sampleRequest)
	if err != nil {
		c.JSON(500, utils.Fail(100, err.Error()))
		return
	}
	logger.Info("End Handler - CreateSampleData")
	c.JSON(200, utils.Send(res))
}

func (h *sampleHandler) HandleGetSampleData(c *gin.Context) {
	logger.Info("Begin Handler - GetSampleData")
	result, err := h.sampleUC.GetSampleData()
	if err != nil {
		c.JSON(400, utils.Fail(100, err.Error()))
		return
	}
	logger.Info("End Handler - GetSampleData")
	c.JSON(200, utils.Send(result))
}
