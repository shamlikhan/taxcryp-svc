package usecase

import (
	"taxcryp/cmd/domain"
	"taxcryp/cmd/usecase/sample"
)

type Usecase struct {
	Sample sample.UsecaseInterface
}

func Init(domain *domain.Domain) *Usecase {
	return &Usecase{
		Sample: sample.InitSampleUsecase(domain.Sample),
	}
}
