package sample

import (
	"taxcryp/cmd/entity/models"
	"taxcryp/cmd/entity/request"
	"taxcryp/logger"
)

func (u *usecase) CreateSampleData(req request.CreateSampleRequest) (result models.Sample, err error) {
	logger.Info("Begin Usecase - CreateSampleData")
	sample := models.Sample{
		Count: req.Count,
	}
	result, err = u.sample.CreateSampleData(sample)
	if err != nil {
		return result, err
	}
	logger.Info("End Usecase - CreateSampleData")
	return result, nil
}

func (u *usecase) GetSampleData() (result []models.Sample, err error) {
	logger.Info("Begin Usecase - GetSampleData")
	result, err = u.sample.GetSampleData()
	if err != nil {
		return result, err
	}
	logger.Info("End Usecase - GetSampleData")
	return result, nil
}
