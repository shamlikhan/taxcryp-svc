package sample

import (
	"errors"
	"fmt"
	"os"
	mock_sample_dom "taxcryp/cmd/domain/sample/mocks"
	"taxcryp/cmd/entity/models"
	"taxcryp/logger"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	setUp()
	code := m.Run()
	fmt.Printf("test exited with code: %d; ok = %v\n", code, code == 0)
	os.Exit(code)
}

func setUp() {
	_, err := logger.InitLogger("TEST")
	if err != nil {
		logger.Errorf("Error in initializing logger in Test", "error", err)
	}
}

func TestGetSampleData(t *testing.T) {
	t.Run("GetSampleData - success", func(t *testing.T) {
		mockSampleDomain := MockGetSampleData()
		sampleUC := InitSampleUsecase(mockSampleDomain)

		result, err := sampleUC.GetSampleData()
		assert.Nil(t, err)
		assert.Equal(t, 2, len(result))
	})
	t.Run("GetSampleData - return error", func(t *testing.T) {
		mockSampleDomain := new(mock_sample_dom.DomainInterface)
		mockSampleDomain.On("GetSampleData").Return(nil, errors.New("mock error"))
		sampleUC := InitSampleUsecase(mockSampleDomain)

		_, err := sampleUC.GetSampleData()
		assert.NotNil(t, err)
	})
}

func MockGetSampleData() *mock_sample_dom.DomainInterface {
	samples := []models.Sample{
		{
			ID:    1,
			Count: 10,
		},
		{
			ID:    2,
			Count: 20,
		},
	}
	sampleDomain := new(mock_sample_dom.DomainInterface)
	sampleDomain.On("GetSampleData").Return(samples, nil)
	return sampleDomain
}
