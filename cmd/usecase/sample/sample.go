package sample

import (
	"taxcryp/cmd/domain/sample"
	"taxcryp/cmd/entity/models"
	"taxcryp/cmd/entity/request"
)

type UsecaseInterface interface {
	GetSampleData() (result []models.Sample, err error)
	CreateSampleData(req request.CreateSampleRequest) (result models.Sample, err error)
}

type usecase struct {
	sample sample.DomainInterface
}

func InitSampleUsecase(sample sample.DomainInterface) UsecaseInterface {
	return &usecase{
		sample: sample,
	}
}
